##################################################
#                                                #
#            Installation for AWS                #
#                                                #
##################################################

AWS_MAIN_URL=$(awk '{print $1}' /root/variables.txt)
AWS_IMAGE_URL=$(awk '{print $2}' /root/variables.txt)
AWS_MAIN=$(awk '{print $3}' /root/variables.txt)
AWS_IMAGE=$(awk '{print $4}' /root/variables.txt)

apt update
apt install -y git awscli nginx

cat <<EOF > /etc/nginx/sites-available/default
server {
        resolver 8.8.8.8;
	listen 8080 default_server;
	listen [::]:8080 default_server;
	root /var/www/html;
	index index.html index.htm index.nginx-debian.html;

	server_name _;
  location / {
      limit_except GET {
          deny all;
      }

      proxy_set_header User-Agent xxxyyyzzz;

      proxy_buffering off;
      proxy_pass https://${AWS_MAIN_URL};
  }

  location ~* ^.+.(jpg|jpeg|png|ico)$ {
      limit_except GET {
          deny all;
      }

      proxy_set_header User-Agent xxxyyyzzz;

      proxy_buffering off;
      proxy_pass https://${AWS_IMAGE_URL};
  }
}
EOF

nginx -s reload

cd /root/
git clone https://github.com/gabrielecirulli/2048.git
aws s3 sync 2048 s3://${AWS_MAIN}
aws s3 sync 2048 s3://${AWS_IMAGE}
